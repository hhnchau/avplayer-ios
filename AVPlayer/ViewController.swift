//
//  ViewController.swift
//  AVPlayer
//
//  Created by MacPro on 08/10/2023.
//https://demo.unified-streaming.com/k8s/features/stable/video/tears-of-steel/tears-of-steel.ism/.m3u8
//https://devstreaming-cdn.apple.com/videos/streaming/examples/img_bipbop_adv_example_fmp4/master.m3u8
//https://github.com/pushpendra996/avplayer-swift-part-3-ios/blob/main/NetflixPlayer/ViewController.swift

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var player: UIView!
    
    @IBOutlet weak var url: UITextField!
    
    private let playerView = Player()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playerView.add(to: player)
    }
    
    
    @IBAction func play(_ sender: Any) {
        guard let link = url.text,
              let url = URL(string: link) else {
                  return
              }
        playerView.play(url: url)
    }
    

    @IBAction func pause(_ sender: Any) {
        playerView.pause()
    }
}

